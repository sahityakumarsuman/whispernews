package k.otp.newsapplication.models;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by sahitya on 7/10/18.
 */

public class TopHeadLines implements Parcelable {
  public static final Creator<TopHeadLines> CREATOR = new Creator<TopHeadLines>() {
    @Override
    public TopHeadLines createFromParcel(Parcel in) {
      return new TopHeadLines(in);
    }

    @Override
    public TopHeadLines[] newArray(int size) {
      return new TopHeadLines[size];
    }
  };
  /**
   * status : ok
   * totalResults : 20
   * articles : [{"source":{"id":"the-new-york-times","name":"The New York Times"},"author":null,"title":"Pompeo Cites 'Good Trip' to North Korea and Progress on a Deal","description":null,"url":"https://www.nytimes.com/2018/10/07/world/asia/pompeo-north-korea-visit.html","urlToImage":null,"publishedAt":"2018-10-07T09:03:09Z","content":null},{"source":{"id":"usa-today","name":"USA Today"},"author":null,"title":"'Brazil's Donald Trump' is presidential frontrunner","description":null,"url":"https://www.usatoday.com/story/news/world/2018/10/07/brazils-donald-trump-leading-presidential-race/1557784002/","urlToImage":null,"publishedAt":"2018-10-07T08:59:44Z","content":null},{"source":{"id":"reuters","name":"Reuters"},"author":null,"title":"Dissatisfied Latvians turn to newcomers in parliamentary election","description":null,"url":"https://uk.reuters.com/article/uk-latvia-election/dissatisfied-latvians-turn-to-newcomers-in-parliamentary-election-idUKKCN1MH0BB","urlToImage":null,"publishedAt":"2018-10-07T08:53:00Z","content":null},{"source":{"id":"newsweek","name":"Newsweek"},"author":"Jason Murdock","title":"Haiti Earthquake: Magnitude 5.9 Tremor Hits\u2014Deaths Recorded, Homes Destroyed","description":"At least 10 people have lost their lives and 130 people are estimated injured, officials said.","url":"https://www.newsweek.com/haiti-earthquake-magnitude-59-tremor-hits-deaths-recorded-homes-destroyed-1156894","urlToImage":"https://s.newsweek.com/sites/www.newsweek.com/files/styles/full/public/2018/10/07/haiti-earthquake.png","publishedAt":"2018-10-07T08:40:16Z","content":"A magnitude 5.9 earthquake struck close to Haiti on the evening of Saturday, October 6, according to the US Geological Survey and local authorities. Data indicated that the quake hit around 19km north west of Port-de-Paix, Haiti. An initial assessment of the \u2026 [+2372 chars]"},{"source":{"id":"reuters","name":"Reuters"},"author":"Reuters Editorial","title":"Indonesia to stop searching for quake victims on Thursday: agency","description":"Indonesian rescue workers will stop searching for the bodies of victims of an earthquake and tsunami on the island of Sulawesi on Thursday, the national disaster mitigation agency said on Sunday.","url":"https://www.reuters.com/article/us-indonesia-quake-search/indonesia-to-stop-searching-for-quake-victims-on-thursday-agency-idUSKCN1MH0AH","urlToImage":"https://s4.reutersmedia.net/resources_v2/images/rcom-default.png","publishedAt":"2018-10-07T08:36:55Z","content":"PALU, Indonesia (Reuters) - Indonesian rescue workers will stop searching for the bodies of victims of an earthquake and tsunami on the island of Sulawesi on Thursday, the national disaster mitigation agency said on Sunday. The official death toll from the Se\u2026 [+289 chars]"},{"source":{"id":"reuters","name":"Reuters"},"author":"Reuters Editorial","title":"Palestinian gunman kills two Israelis in West Bank: Israeli military","description":"A Palestinian gunman shot dead two Israelis and wounded a third on Sunday in an industrial park next to a Jewish settlement in the occupied West Bank, the Israeli military said.","url":"https://www.reuters.com/article/us-israel-palestinians-shooting/palestinian-gunman-kills-two-israelis-in-west-bank-israeli-military-idUSKCN1MH05Y","urlToImage":"https://s1.reutersmedia.net/resources/r/?m=02&d=20181007&t=2&i=1312116718&w=1200&r=LYNXNPEE960D1","publishedAt":"2018-10-07T08:19:42Z","content":"BARKAN, West Bank (Reuters) - A Palestinian gunman shot dead two Israelis and wounded a third on Sunday in an industrial park next to a Jewish settlement in the occupied West Bank, the Israeli military said. The assailant, the military said, had worked in a f\u2026 [+1190 chars]"},{"source":{"id":"reuters","name":"Reuters"},"author":"Edward McAllister","title":"Cameroon goes to the polls as Biya seeks to extend 36-year rule","description":"Cameroonians head to the polls on Sunday in an election widely expected to extend the 36-year rule of President Paul Biya and confirm his place as one of Africa's last multi-decade leaders.","url":"https://in.reuters.com/article/cameroon-election/cameroon-goes-to-the-polls-as-biya-seeks-to-extend-36-year-rule-idINKCN1MH08X","urlToImage":"https://s3.reutersmedia.net/resources/r/?m=02&d=20181007&t=2&i=1312106211&w=1200&r=LYNXNPEE960AO","publishedAt":"2018-10-07T07:34:00Z","content":"YAOUNDE (Reuters) - Cameroonians head to the polls on Sunday in an election widely expected to extend the 36-year rule of President Paul Biya and confirm his place as one of Africa\u2019s last multi-decade leaders. A victory for Biya, who has ruled since 1982, wou\u2026 [+3998 chars]"},{"source":{"id":"the-new-york-times","name":"The New York Times"},"author":null,"title":"On 'SNL,' Republican Senators Host a Locker Room Celebration for Kavanaugh's Confirmation","description":"Plus, Pete Davidson addresses Kanye West\u2019s impromptu post-credits speech.","url":"https://www.nytimes.com/2018/10/07/arts/television/snl-kavanaugh-confirmation-celebration-awkwafina-hosts.html","urlToImage":"https://static01.nyt.com/images/2018/10/08/arts/television/08snl/08snl-facebookJumbo.jpg","publishedAt":"2018-10-07T07:29:14Z","content":"Kate McKinnon, returning as Lindsey Graham, proudly asked, \u201cHow amazing is this? We made a lot of women real worried today, but I\u2019m not getting pregnant so I don\u2019t care.\u201d Cecily Strong appeared as Susan Collins, who announced her support for Kavanaugh in a sp\u2026 [+938 chars]"},{"source":{"id":null,"name":"Espn.com"},"author":null,"title":"Yankees' Gary Sanchez silences Red Sox with 479-foot blast","description":"Yankees slugger Gary Sanchez fueled a 6-2 win over the Red Sox that evened their ALDS series at 1-1 with two towering home runs over the Green Monster at Fenway Park.","url":"http://www.espn.com/mlb/story/_/id/24915672/gary-sanchez-lifts-new-york-yankees-479-foot-moonshot","urlToImage":"http://a.espncdn.com/combiner/i?img=%2Fphoto%2F2018%2F0928%2Fr438657_1296x729_16%2D9.jpg","publishedAt":"2018-10-07T06:28:42Z","content":"BOSTON -- Forty years to the day after New York Yankees catching legend Thurman Munson launched a pivotal, go-ahead home run in Game 3 of the 1978 ALCS, the Bronx Bombers' current catcher, Gary Sanchez, had a similarly special postseason day at the plate. Tha\u2026 [+5241 chars]"},{"source":{"id":null,"name":"Cbssports.com"},"author":"","title":"UFC 229 results -- Khabib vs. McGregor: Live updates, fight card, start time, highlights","description":"Khabib Nurmagomedov retained his lightweight title via submission, but that was just the start","url":"https://www.cbssports.com/mma/news/ufc-229-results-khabib-vs-mcgregor-live-updates-fight-card-start-time-highlights/","urlToImage":"https://sportshub.cbsistatic.com/i/r/2018/10/07/15972a98-a191-4e18-9362-106bc7c6124f/thumbnail/770x433/ec56fc677b180de02981fc77ead6af47/khabib-mcgregor-ground.jpg","publishedAt":"2018-10-07T05:03:00Z","content":"LAS VEGAS -- With the location intact, Khabib Nurmagomedov attempted to settle his grudge war with Conor McGregor at UFC 229 on Saturday and did everything he said he would in the lead-up. Nurmagomedov (27-0) relied on his superior grappling skills to maul th\u2026 [+5760 chars]"},{"source":{"id":null,"name":"Espn.com"},"author":null,"title":"Wild finishes, tons of offense and plenty of fun: Welcome to the Big 12","description":"Texas outscores Oklahoma for a memorable Week 6 win that says lots about the conference, Alabama still isn't satisfied and Clemson showcases its running game.","url":"http://www.espn.com/college-football/story/_/page/gamedayfinal100618/welcome-big-12","urlToImage":"http://a2.espncdn.com/combiner/i?img=%2Fphoto%2F2018%2F1006%2Fr443191_1296x729_16%2D9.jpg","publishedAt":"2018-10-07T04:34:06Z","content":"The most interesting conference in college football is the Big 12, and it's not really close. To be sure, we're not saying the Big 12 is the best league. The Big 12 is like a \"Mission Impossible\" movie. It's good, maybe great. But that's beside the point. Wha\u2026 [+11821 chars]"},{"source":{"id":null,"name":"People.com"},"author":"Breanne L. Heldman","title":"The Walking Dead Actor Scott Wilson Dies at 76 Due to Complications from Leukemia","description":"The star also appeared in a number of classic movies, including In Cold Blood and The Great Gatsby opposite Robert Redford","url":"https://people.com/tv/scott-wilson-dies-walking-dead-star-dead/","urlToImage":"https://peopledotcom.files.wordpress.com/2018/10/gettyimages-492065734.jpg?crop=462px%2C106px%2C4187px%2C2199px&resize=1200%2C630","publishedAt":"2018-10-07T03:21:37Z","content":null},{"source":{"id":"the-new-york-times","name":"The New York Times"},"author":null,"title":"A Day of Broad Smiles and Raised Thumbs for Trump","description":"\u201cThere\u2019s nobody with a squeaky clean past like Brett Kavanaugh,\u201d Mr. Trump told reporters, dismissing allegations of sexual assault and misconduct against the judge.","url":"https://www.nytimes.com/2018/10/06/us/politics/trump-kavanaugh.html","urlToImage":"https://static01.nyt.com/images/2018/10/07/us/politics/07DC-TRUMP-01/07DC-TRUMP-01-facebookJumbo.jpg","publishedAt":"2018-10-07T02:41:55Z","content":"\u201cEach of you will have the chance to render your verdict on the Democrats\u2019 conduct at the ballot box,\u201d Mr. Trump said. He targeted Senator Richard Blumenthal, Democrat of Connecticut, as \u201cDa Nang Dick\u201d for falsehoods about his military service during the Viet\u2026 [+1476 chars]"},{"source":{"id":"cnn","name":"CNN"},"author":"Amir Vera, CNN","title":"Central American storm may become tropical depression in coming days, hurricane center says","description":"A tropical storm watch is in effect for the coast of Mexico from Tulum to Cabo Catoche.","url":"https://www.cnn.com/2018/10/06/weather/potential-tropical-cyclone-14-wxc/index.html","urlToImage":"https://cdn.cnn.com/cnnnext/dam/assets/181006203619-potential-tropical-cyclone-14-super-tease.jpg","publishedAt":"2018-10-07T01:31:18Z","content":null},{"source":{"id":"the-new-york-times","name":"The New York Times"},"author":null,"title":"Turkey Believes Prominent Saudi Critic Was Killed in Saudi Consulate in Istanbul","description":"The critic, Jamal Khashoggi, entered the consulate on Tuesday to obtain a document he needed to get married and never emerged.","url":"https://www.nytimes.com/2018/10/06/world/turkey-believes-prominent-saudi-critic-was-killed-in-saudi-consulate-in-istanbul.html","urlToImage":"https://static01.nyt.com/images/2018/10/07/world/07TURKEY-02/merlin_144862557_330722e8-c44c-4eb7-ad3b-67675951adde-facebookJumbo.jpg","publishedAt":"2018-10-07T00:09:26Z","content":"Earlier on Saturday, Turkey\u2019s semiofficial Anadolu news agency reported that 15 Saudi citizens, including Saudi diplomats, had arrived in Istanbul on two separate planes and were at the consulate around the same time as Mr. Khashoggi on Tuesday. They later le\u2026 [+1192 chars]"},{"source":{"id":null,"name":"Gizmodo.com"},"author":null,"title":"Capping Off Endless Leaks, the Pixel 3 XL Is Already on Sale in Hong Kong Before Launch Event","description":null,"url":"https://gizmodo.com/capping-off-endless-leaks-the-pixel-3-xl-is-already-on-1829577936","urlToImage":null,"publishedAt":"2018-10-06T23:40:44Z","content":null},{"source":{"id":"the-new-york-times","name":"The New York Times"},"author":null,"title":"Does Melania Trump Ever Tell the President to Put Away His Phone? 'Yes!'","description":"In front of the Great Sphinx in Egypt, Mrs. Trump took questions on Brett Kavanaugh, the president\u2019s remarks about Africa and his Twitter habits.","url":"https://www.nytimes.com/2018/10/06/world/africa/melania-trump-africa-trip-egypt.html","urlToImage":"https://static01.nyt.com/images/2018/10/07/world/07flotus/07flotus-facebookJumbo.jpg","publishedAt":"2018-10-06T21:24:00Z","content":"CAIRO \u2014 No one can say Melania Trump, the first lady, doesn\u2019t understand the power of an image. Standing in front of one of the best known but most enigmatic monuments of the ancient world, the Great Sphinx, Mrs. Trump \u2014 one of the most mysterious first ladie\u2026 [+817 chars]"},{"source":{"id":"reuters","name":"Reuters"},"author":"Ingrid Melander","title":"French police probe Interpol chief's disappearance on China trip","description":"French police are investigating the disappearance of Interpol chief, Meng Hongwei, who was reported missing after travelling from France to his native China, while his wife has been placed under police protection after receiving threats.","url":"https://in.reuters.com/article/france-interpol-missing/french-police-probe-interpol-chiefs-disappearance-on-china-trip-idINKCN1MF1BR","urlToImage":"https://s3.reutersmedia.net/resources/r/?m=02&d=20181005&t=2&i=1311550852&w=1200&r=LYNXNPEE940ZQ","publishedAt":"2018-10-06T14:28:38Z","content":"PARIS (Reuters) - French police are investigating the disappearance of Interpol chief, Meng Hongwei, who was reported missing after travelling from France to his native China, while his wife has been placed under police protection after receiving threats. Men\u2026 [+4031 chars]"},{"source":{"id":null,"name":"Dallasnews.com"},"author":"Robert T. Garrett","title":"Beto O'Rourke, Ted Cruz struggle to capture loyalty of young Texans","description":"AUSTIN -- In the Texas Senate race, Beto O\u2019Rourke and Ted Cruz are vying for young hearts and minds.By outward appearances, O\u2019Rourke is...","url":"https://www.dallasnews.com/news/2018-elections/2018/10/06/beto-orourke-ted-cruz-struggle-captureloyalty-young-texans","urlToImage":"https://dallasnews.imgix.net/1538781962-BETOO_ROURKERALLY_BE2906_65088834.JPG?w=1200&h=630&format=jpg&crop=faces&fit=crop","publishedAt":"2018-10-06T12:30:32Z","content":"AUSTIN -- In the Texas Senate race, Beto O\u2019Rourke and Ted Cruz are vying for young hearts and minds. By outward appearances, O\u2019Rourke is winning. In his week-long dash across a dozen college campuses in advance of Tuesday\u2019s voter-registration deadline, the De\u2026 [+3354 chars]"},{"source":{"id":"wired","name":"Wired"},"author":"WIRED Staff","title":"Don't Buy the Trump Administration's China Misdirection","description":"The White House keeps accusing China of election interference\u2014but it's nothing like Russia in 2016.","url":"https://www.wired.com/story/dont-buy-trump-administration-china-misdirection/","urlToImage":"https://media.wired.com/photos/5bb5455980ec002859e56b45/191:100/pass/ChinaTrumpMyth-1025198492.jpg","publishedAt":"2018-10-06T11:01:24Z","content":"Near the end of September, before the United Nations, President Donald Trump leveled an extraordinary charge: China was attempting to \u201cmeddle\u201d and \u201cinterfere\u201d in the upcoming US election. A senior intelligence official repeated the claim on a subsequent call \u2026 [+6485 chars]"}]
   */

  @SerializedName("ok")
  private String status;
  @SerializedName("totalResults")
  private int totalResults;
  @SerializedName("articles")
  private ArrayList<ArticlesData> articles_list;

  protected TopHeadLines(Parcel in) {
    status = in.readString();
    totalResults = in.readInt();
    articles_list = in.createTypedArrayList(ArticlesData.CREATOR);
  }

  public String getStatus() {
    return status;
  }

  public void setStatus(String status) {
    this.status = status;
  }

  public int getTotalResults() {
    return totalResults;
  }

  public void setTotalResults(int totalResults) {
    this.totalResults = totalResults;
  }

  public ArrayList<ArticlesData> getArticles_list() {
    return articles_list;
  }

  public void setArticles_list(ArrayList<ArticlesData> articles_list) {
    this.articles_list = articles_list;
  }

  @Override
  public int describeContents() {
    return 0;
  }

  @Override
  public void writeToParcel(Parcel dest, int flags) {
    dest.writeString(status);
    dest.writeInt(totalResults);
    dest.writeTypedList(articles_list);
  }

  public static class ArticlesData implements Parcelable {
    public static final Creator<ArticlesData> CREATOR = new Creator<ArticlesData>() {
      @Override
      public ArticlesData createFromParcel(Parcel in) {
        return new ArticlesData(in);
      }

      @Override
      public ArticlesData[] newArray(int size) {
        return new ArticlesData[size];
      }
    };
    /**
     * source : {"id":"the-new-york-times","name":"The New York Times"}
     * author : null
     * title : Pompeo Cites 'Good Trip' to North Korea and Progress on a Deal
     * description : null
     * url : https://www.nytimes.com/2018/10/07/world/asia/pompeo-north-korea-visit.html
     * urlToImage : null
     * publishedAt : 2018-10-07T09:03:09Z
     * content : null
     */

    @SerializedName("source")
    private SourceBean source;
    @SerializedName("author")
    private String author;
    @SerializedName("title")
    private String title;
    @SerializedName("description")
    private String description;
    @SerializedName("url")
    private String url;
    @SerializedName("urlToImage")
    private String urlToImage;
    @SerializedName("publishedAt")
    private String publishedAt;
    @SerializedName("content")
    private String content;

    protected ArticlesData(Parcel in) {
      author = in.readString();
      title = in.readString();
      url = in.readString();
      publishedAt = in.readString();
    }

    public SourceBean getSource() {
      return source;
    }

    public void setSource(SourceBean source) {
      this.source = source;
    }

    public String getAuthor() {
      return author;
    }

    public void setAuthor(String author) {
      this.author = author;
    }

    public String getTitle() {
      return title;
    }

    public void setTitle(String title) {
      this.title = title;
    }

    public String getDescription() {
      return description;
    }

    public void setDescription(String description) {
      this.description = description;
    }

    public String getUrl() {
      return url;
    }

    public void setUrl(String url) {
      this.url = url;
    }

    public String getUrlToImage() {
      return urlToImage;
    }

    public void setUrlToImage(String urlToImage) {
      this.urlToImage = urlToImage;
    }

    public String getPublishedAt() {
      return publishedAt;
    }

    public void setPublishedAt(String publishedAt) {
      this.publishedAt = publishedAt;
    }

    public String getContent() {
      return content;
    }

    public void setContent(String content) {
      this.content = content;
    }

    @Override
    public int describeContents() {
      return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
      dest.writeString(author);
      dest.writeString(title);
      dest.writeString(url);
      dest.writeString(publishedAt);
    }

    public static class SourceBean {
      /**
       * id : the-new-york-times
       * name : The New York Times
       */

      @SerializedName("id")
      private String id;
      @SerializedName("name")
      private String name;

      public String getId() {
        return id;
      }

      public void setId(String id) {
        this.id = id;
      }

      public String getName() {
        return name;
      }

      public void setName(String name) {
        this.name = name;
      }
    }
  }
}
