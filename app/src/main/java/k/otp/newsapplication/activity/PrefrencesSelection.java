package k.otp.newsapplication.activity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.widget.TextView;

import java.nio.channels.SelectableChannel;
import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import k.otp.newsapplication.MainActivity;
import k.otp.newsapplication.R;
import k.otp.newsapplication.adapters.PrefrencesAdapter;
import k.otp.newsapplication.utils.Constants;
import k.otp.newsapplication.utils.Helper;
import timber.log.Timber;

/**
 * Created by sahitya on 7/10/18.
 */

public class PrefrencesSelection extends AppCompatActivity {


  @BindView(R.id.user_prefrences_list)
  RecyclerView user_prefrences_list;

  @BindView(R.id.start_tv)
  TextView start_tv;
  PrefrencesAdapter _mPrefAdapter = null;
  ArrayList<String> array_workout = new ArrayList<>();
  private GridLayoutManager layoutManager;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.prefrnces_screen_layout_activity);
    ButterKnife.bind(this);

    array_workout.add(Constants.USER_PREF_ALL);
    array_workout.add(Constants.USER_PREF_BUISNESS);
    array_workout.add(Constants.USER_PREF_ENTERTAINMENT);
    array_workout.add(Constants.USER_PREF_SPORTS);
    array_workout.add(Constants.USER_PREF_COMICS);
    array_workout.add(Constants.USER_PREF_TECHNOLOGY);

    layoutManager = new GridLayoutManager(this, 2);
    user_prefrences_list.setHasFixedSize(true);
    user_prefrences_list.setLayoutManager(layoutManager);
    _mPrefAdapter = new PrefrencesAdapter(this, array_workout);
    user_prefrences_list.setAdapter(_mPrefAdapter);
  }

  @OnClick(R.id.start_tv)
  void start_activity() {
    startActivity(new Intent(this, MainActivity.class));
  }

}
