package k.otp.newsapplication.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import k.otp.newsapplication.R;
import k.otp.newsapplication.app.NewsApplication;

/**
 * Created by sahitya on 7/10/18.
 */

public class ProfileFragment extends Fragment {

  @BindView(R.id.fab)
  FloatingActionButton fab;

  private Unbinder unbinder = null;

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.my_profile, container, false);
    unbinder = ButterKnife.bind(this, view);
    return view;
  }

  @OnClick(R.id.fab)
  void show_thanks() {
    Toast.makeText(getContext(), "Thank You", Toast.LENGTH_SHORT).show();
  }

  @Override
  public void onDestroyView() {
    if (unbinder != null)
      unbinder.unbind();
    super.onDestroyView();
  }

}
