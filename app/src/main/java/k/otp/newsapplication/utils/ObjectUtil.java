package k.otp.newsapplication.utils;

import java.util.List;



/*
* Personal fav. class to change other workout
* */
public class ObjectUtil {
  public static boolean isEmpty(String string) {
    return string == null || string.length() == 0;
  }

  public static boolean isNull(Object obj) {
    return obj == null;
  }

  public static boolean isEmptyList(List list) {
    return list == null || list.isEmpty();
  }
}
