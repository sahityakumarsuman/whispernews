package k.otp.newsapplication;

import android.content.Intent;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.AppCompatTextView;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.androidnetworking.AndroidNetworking;
import com.androidnetworking.common.Priority;
import com.androidnetworking.error.ANError;
import com.androidnetworking.interfaces.ParsedRequestListener;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import k.otp.newsapplication.activity.PrefrencesSelection;
import k.otp.newsapplication.adapters.VerticalViewPagerAdapter;
import k.otp.newsapplication.models.TopHeadLines;
import k.otp.newsapplication.utils.CommonMethods;
import k.otp.newsapplication.utils.Constants;
import k.otp.newsapplication.utils.Helper;
import k.otp.newsapplication.utils.ResourcesUtil;
import k.otp.newsapplication.viewpager.VerticalViewPager;
import timber.log.Timber;

public class MainActivity extends AppCompatActivity {

  public VerticalViewPagerAdapter viewPagerAdapter = null;

  @BindView(R.id.main_content)
  RelativeLayout main_content;

  @BindView(R.id.internet_connection_not_RL)
  RelativeLayout internet_connection_not_RL;

  @BindView(R.id.retry_button)
  AppCompatTextView retry_button;

  @BindView(R.id.progress_view_layout)
  RelativeLayout progress_view_layout;

  @BindView(R.id.viewPager)
  VerticalViewPager viewPager;

  @BindView(R.id.show_prefrences)
  FloatingActionButton show_prefrences;


  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.activity_main);
    ButterKnife.bind(this);

    show_user_selected_pref();


    //@checking internet connection for preforming activity
    if (CommonMethods.isConnected(MainActivity.this)) {
      internet_connection_not_RL.setVisibility(View.GONE);
      progress_view_layout.setVisibility(View.GONE);
      main_content.setVisibility(View.VISIBLE);
      call_api_for_info();
    } else {
      main_content.setVisibility(View.GONE);
      progress_view_layout.setVisibility(View.GONE);
      internet_connection_not_RL.setVisibility(View.VISIBLE);
    }
  }

  //@Showing user what category he had selected for news preferences
  private void show_user_selected_pref() {
    String user_pref_to_show = "";
    ArrayList<String> user_pref = Helper.loadArray(Constants.USER_CHOOSED_PREFRENCES_NAME, MainActivity.this);
    for (int i = 0; i < user_pref.size(); i++)
      user_pref_to_show += user_pref.get(i) + ", ";
    Toast.makeText(this, "You Selected " + user_pref_to_show, Toast.LENGTH_SHORT).show();
  }


  //Let user change preferences
  @OnClick(R.id.show_prefrences)
  void change_prefrences() {
    startActivity(new Intent(MainActivity.this, PrefrencesSelection.class));
    finish();
  }


  //Let user retry to refresh the feed in case of Internet connection lost
  @OnClick(R.id.retry_button)
  void retry_connection() {
    if (CommonMethods.isConnected(MainActivity.this)) {
      internet_connection_not_RL.setVisibility(View.GONE);
      progress_view_layout.setVisibility(View.GONE);
      main_content.setVisibility(View.VISIBLE);
      call_api_for_info();
    } else {
      progress_view_layout.setVisibility(View.GONE);
      main_content.setVisibility(View.GONE);
      internet_connection_not_RL.setVisibility(View.VISIBLE);
    }
  }


  // Calling api of top Headlines to show user feed about it
  private void call_api_for_info() {
    internet_connection_not_RL.setVisibility(View.GONE);
    main_content.setVisibility(View.GONE);
    progress_view_layout.setVisibility(View.VISIBLE);


    String API_CALL = Constants.TOP_HEAD_LINES;
    AndroidNetworking.get(API_CALL)
            .setPriority(Priority.IMMEDIATE)
            .build()
            .getAsObject(TopHeadLines.class, new ParsedRequestListener<TopHeadLines>() {
              @Override
              public void onResponse(TopHeadLines response) {
                if (response != null && response.getArticles_list() != null) {
                  progress_view_layout.setVisibility(View.GONE);
                  internet_connection_not_RL.setVisibility(View.GONE);
                  main_content.setVisibility(View.VISIBLE);
                  viewPagerAdapter = new VerticalViewPagerAdapter(getSupportFragmentManager(), response.getArticles_list(), MainActivity.this);
                  viewPager.setAdapter(viewPagerAdapter);
                } else {
                  progress_view_layout.setVisibility(View.GONE);
                  main_content.setVisibility(View.GONE);
                  internet_connection_not_RL.setVisibility(View.VISIBLE);
                  Toast.makeText(MainActivity.this, "Server Lost Error, Retry!", Toast.LENGTH_SHORT).show();
                }
              }

              @Override
              public void onError(ANError anError) {
                Timber.d("Error ::" + anError.getErrorDetail());
                main_content.setVisibility(View.GONE);
                progress_view_layout.setVisibility(View.GONE);
                internet_connection_not_RL.setVisibility(View.VISIBLE);
              }
            });
  }

}
