package k.otp.newsapplication.fragments;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebChromeClient;
import android.webkit.WebResourceError;
import android.webkit.WebResourceRequest;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;
import android.widget.TextView;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import k.otp.newsapplication.R;
import k.otp.newsapplication.utils.ObjectUtil;
import timber.log.Timber;

/**
 * Created by sahitya on 7/10/18.
 */

public class FullBrowserFragment extends Fragment {
  @BindView(R.id.webView)
  WebView webView;
  @BindView(R.id.progressBar)
  ProgressBar progressBar;

  @BindView(R.id.loading_url_tv)
  TextView loading_url_tv;

  private float m_downX;
  private Unbinder unbinder = null;

  @Override
  public void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
  }

  @Override
  public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
    super.onCreateContextMenu(menu, v, menuInfo);
  }

  @Nullable
  @Override
  public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.browser_view_layout_fragment, container, false);
    unbinder = ButterKnife.bind(this, view);
    Bundle bundle = getArguments();
    String url = bundle.getString("url");
    initWebView();

    loading_url_tv.setText(url);

    webView.loadUrl(url);
    return view;
  }

  private void initWebView() {
    webView.setWebChromeClient(new MyWebChromeClient(getContext()));
    webView.setWebViewClient(new WebViewClient() {
      @Override
      public void onPageStarted(WebView view, String url, Bitmap favicon) {
        super.onPageStarted(view, url, favicon);
        if (!ObjectUtil.isNull(progressBar))
          progressBar.setVisibility(View.VISIBLE);

      }

      @Override
      public boolean shouldOverrideUrlLoading(WebView view, String url) {
        if (!ObjectUtil.isNull(webView))
          webView.loadUrl(url);
        return true;
      }

      @Override
      public void onPageFinished(WebView view, String url) {
        super.onPageFinished(view, url);
        if (!ObjectUtil.isNull(progressBar))
          progressBar.setVisibility(View.GONE);

      }

      @Override
      public void onReceivedError(WebView view, WebResourceRequest request, WebResourceError error) {
        super.onReceivedError(view, request, error);
        if (!ObjectUtil.isNull(progressBar))
          progressBar.setVisibility(View.GONE);

      }
    });
    webView.clearCache(true);
    webView.clearHistory();
    webView.getSettings().setJavaScriptEnabled(true);
    webView.setHorizontalScrollBarEnabled(false);
    webView.setOnTouchListener(new View.OnTouchListener() {
      public boolean onTouch(View v, MotionEvent event) {

        if (event.getPointerCount() > 1) {
          //Multi touch detected
          return true;
        }

        switch (event.getAction()) {
          case MotionEvent.ACTION_DOWN: {
            // save the x
            m_downX = event.getX();
          }
          break;

          case MotionEvent.ACTION_MOVE:
          case MotionEvent.ACTION_CANCEL:
          case MotionEvent.ACTION_UP: {
            // set x so that it doesn't move
            event.setLocation(m_downX, event.getY());
          }
          break;
        }

        return false;
      }
    });
  }

  @Override
  public void onDestroyView() {
    if (unbinder != null)
      unbinder.unbind();
    super.onDestroyView();
  }

  private class MyWebChromeClient extends WebChromeClient {
    Context context;

    public MyWebChromeClient(Context context) {
      super();
      this.context = context;
    }
  }
}
