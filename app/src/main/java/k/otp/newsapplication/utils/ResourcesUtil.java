package k.otp.newsapplication.utils;

import android.content.Context;
import android.content.res.ColorStateList;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;


import k.otp.newsapplication.app.NewsApplication;

import static android.os.Build.VERSION.SDK_INT;
import static android.os.Build.VERSION_CODES.LOLLIPOP;



/*
* Common class which handles all API version of resources at one place
* */
public class ResourcesUtil {
  private static Context context = NewsApplication.getInstance().getApplicationContext();
  private static Resources.Theme theme = NewsApplication.getInstance().getApplicationContext().getTheme();

  public static Drawable getDrawableById(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getDrawable(resId, theme) :
            context.getResources().getDrawable(resId);
  }

  public static String getString(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getString(resId) :
            context.getResources().getString(resId);
  }

  public static int getColor(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getColor(resId) :
            context.getResources().getColor(resId);
  }


  public static ColorStateList getColorStateList(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getColorStateList(resId) :
            context.getResources().getColorStateList(resId);
  }


  public static float getDimen(int resId) {
    return SDK_INT >= LOLLIPOP ? context.getResources().getDimension(resId) :
            context.getResources().getDimension(resId);
  }
}
