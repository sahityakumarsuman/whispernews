package k.otp.newsapplication.utils;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by sahitya on 30/9/18.
 */

public class CommonMethods {

  public static boolean isConnected(final Context context) {
    NetworkInfo info = getNetworkInfo(context);
    return (info != null && info.isConnected());
  }

  public static NetworkInfo getNetworkInfo(final Context context) {
    ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
    return cm.getActiveNetworkInfo();
  }
}
