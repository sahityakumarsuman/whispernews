package k.otp.newsapplication.utils;

/**
 * Created by sahitya on 30/9/18.
 */

public class Constants {


  public static final String TOP_HEAD_LINES = "https://newsapi.org/v2/top-headlines?country=us&apiKey=3a32acf057be4030850ade6b8bb871a4";

  public static final String USER_CHOOSED_PREFRENCES_NAME = "user_selected";
  public static final String USER_PREF_SPORTS = "Sports";
  public static final String USER_PREF_ENTERTAINMENT = "Entertainment";
  public static final String USER_PREF_BUISNESS = "Business";
  public static final String USER_PREF_COMICS = "Comics";
  public static final String USER_PREF_ALL = "All";
  public static final String USER_PREF_TECHNOLOGY = "Technology";


}
