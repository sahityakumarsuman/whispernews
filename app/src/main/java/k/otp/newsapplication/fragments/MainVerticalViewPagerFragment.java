package k.otp.newsapplication.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import k.otp.newsapplication.R;
import k.otp.newsapplication.adapters.HorizontalViewPagerAdapter;
import k.otp.newsapplication.models.TopHeadLines;
import k.otp.newsapplication.utils.ObjectUtil;
import k.otp.newsapplication.viewpager.HorizontalViewPager;


public class MainVerticalViewPagerFragment extends Fragment {


  public HorizontalViewPagerAdapter horizontalViewPagerAdapter = null;
  public String parentInd = null;
  @BindView(R.id.vpHorizontal)
  HorizontalViewPager viewPager;
  private TopHeadLines.ArticlesData _mSingleNewsData = null;
  private Unbinder unbinder = null;

  public MainVerticalViewPagerFragment() {
    // Required empty public constructor
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_content, container, false);

    unbinder = ButterKnife.bind(this, view);
    parentInd = getArguments().getString("parent");
    _mSingleNewsData = getArguments().getParcelable("single_news_details");
    if (ObjectUtil.isNull(horizontalViewPagerAdapter))
      horizontalViewPagerAdapter = new HorizontalViewPagerAdapter(getChildFragmentManager(), _mSingleNewsData);
    horizontalViewPagerAdapter.setParentID(parentInd);
    viewPager.setAdapter(horizontalViewPagerAdapter);
    return view;
  }


  @Override
  public void onDestroyView() {
    if (!ObjectUtil.isNull(unbinder))
      unbinder.unbind();
    super.onDestroyView();
  }
}
