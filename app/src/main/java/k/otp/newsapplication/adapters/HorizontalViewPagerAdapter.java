package k.otp.newsapplication.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import k.otp.newsapplication.fragments.NewsFragmentChild;
import k.otp.newsapplication.fragments.FullBrowserFragment;
import k.otp.newsapplication.models.TopHeadLines;
import k.otp.newsapplication.utils.ObjectUtil;


public class HorizontalViewPagerAdapter extends FragmentStatePagerAdapter {

  public String parentId = null;

  private TopHeadLines.ArticlesData _mSingleNewsData = null;

  public HorizontalViewPagerAdapter(FragmentManager fm, TopHeadLines.ArticlesData singleNewsData) {
    super(fm);
    this._mSingleNewsData = singleNewsData;
  }

  public void setParentID(String parentID) {
    this.parentId = parentID;
  }

  @Override
  public Fragment getItem(int position) {
    if (position == 0) {
      NewsFragmentChild childFragment = new NewsFragmentChild();
      Bundle bundle = new Bundle();
      bundle.putParcelable("single_news_details", _mSingleNewsData);
      bundle.putString("parent", parentId);
      bundle.putString("child", String.valueOf(position));
      childFragment.setArguments(bundle);
      return childFragment;
    } else if (!ObjectUtil.isNull(_mSingleNewsData.getUrl()) && !_mSingleNewsData.getUrl().isEmpty()) {
      FullBrowserFragment childFragment = new FullBrowserFragment();
      Bundle bundle = new Bundle();
      bundle.putString("url", _mSingleNewsData.getUrl());
      bundle.putString("child", String.valueOf(position));
      childFragment.setArguments(bundle);
      return childFragment;
    }
    return null;
  }

  @Override
  public int getCount() {
    if (!ObjectUtil.isNull(_mSingleNewsData)) {
      if (!ObjectUtil.isNull(_mSingleNewsData.getUrl()) && !_mSingleNewsData.getUrl().isEmpty()) {
        return 2;
      } else {
        return 1;
      }
    } else {
      return 0;
    }
  }
}
