package k.otp.newsapplication.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.bumptech.glide.request.RequestOptions;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;
import k.otp.newsapplication.R;
import k.otp.newsapplication.app.NewsApplication;
import k.otp.newsapplication.models.TopHeadLines;
import k.otp.newsapplication.utils.ObjectUtil;


public class NewsFragmentChild extends Fragment {

  @BindView(R.id.news_title)
  TextView news_title;

  @BindView(R.id.news_time)
  TextView news_time;

  @BindView(R.id.textview_description)
  TextView textview_description;

  @BindView(R.id.news_image)
  ImageView news_image;

  @BindView(R.id.share_news_button)
  FloatingActionButton share_news_button;


  private Unbinder unbinder = null;

  private TopHeadLines.ArticlesData _mSingleNewsData = null;

  public NewsFragmentChild() {
    // Required empty public constructor
  }


  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container,
                           Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_child, container, false);
    unbinder = ButterKnife.bind(this, view);
    _mSingleNewsData = getArguments().getParcelable("single_news_details");
    if (_mSingleNewsData != null) {

      news_title.setText(_mSingleNewsData.getTitle() != null ? _mSingleNewsData.getTitle() : ".....");
      news_time.setText(_mSingleNewsData.getPublishedAt() != null ? _mSingleNewsData.getPublishedAt().substring(0, 10) : ".....");
      textview_description.setText(_mSingleNewsData.getContent() != null ? _mSingleNewsData.getContent() : ".....");

      if (_mSingleNewsData.getUrlToImage() != null && !_mSingleNewsData.getUrlToImage().isEmpty()) {
        RequestOptions requestOptions = new RequestOptions();
        requestOptions.fitCenter();
        requestOptions.diskCacheStrategy(DiskCacheStrategy.RESOURCE);
        Glide.with(NewsApplication.getApplicationInstance()
                .getApplicationContext())
                .setDefaultRequestOptions(requestOptions)
                .load(_mSingleNewsData.getUrlToImage())
                .into(news_image);
      }
    }
    return view;
  }


  // Sharing news with other on different platform
  @OnClick(R.id.share_news_button)
  void share_news() {
    Intent share = new Intent(android.content.Intent.ACTION_SEND);
    share.setType("text/plain");
    share.addFlags(Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET);
    share.putExtra(Intent.EXTRA_SUBJECT, "Be Updated with Wishpers App");
    share.putExtra(Intent.EXTRA_TEXT, _mSingleNewsData.getTitle() + "\n\n - To read full article visit " + _mSingleNewsData.getUrl());
    startActivity(Intent.createChooser(share, "Share Wishpers News with..."));
  }

  @Override
  public void onDestroyView() {
    if (!ObjectUtil.isNull(unbinder))
      unbinder.unbind();
    super.onDestroyView();
  }

}
