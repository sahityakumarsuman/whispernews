package k.otp.newsapplication.utils;

import android.content.SharedPreferences;
import android.util.Log;
import android.content.Context;

import java.util.ArrayList;


public class Helper {

  public static void log(String msg) {
    Log.e("Debugger_Key", msg);
  }


  // Saving user pref. to update feed
  public static boolean saveArray(ArrayList<String> array, String arrayName, Context mContext) {
    SharedPreferences prefs = mContext.getSharedPreferences("user_selected_preferences", 0);
    SharedPreferences.Editor editor = prefs.edit();
    editor.putInt(arrayName + "_size", array.size());
    for (int i = 0; i < array.size(); i++)
      editor.putString(arrayName + "_" + i, array.get(i));
    return editor.commit();
  }


  // getting user pref to update user feed
  public static ArrayList<String> loadArray(String arrayName, Context mContext) {
    SharedPreferences prefs = mContext.getSharedPreferences("user_selected_preferences", 0);
    int size = prefs.getInt(arrayName + "_size", 0);
    ArrayList<String> array = new ArrayList(size);
    for (int i = 0; i < size; i++)
      array.add(i, prefs.getString(arrayName + "_" + i, null));
    return array;
  }


}
