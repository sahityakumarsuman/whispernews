package k.otp.newsapplication.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.ArrayList;

import k.otp.newsapplication.fragments.MainVerticalViewPagerFragment;
import k.otp.newsapplication.fragments.ProfileFragment;
import k.otp.newsapplication.models.TopHeadLines;
import k.otp.newsapplication.utils.ObjectUtil;
import timber.log.Timber;

import android.content.Context;

public class VerticalViewPagerAdapter extends FragmentStatePagerAdapter {


  private ArrayList<TopHeadLines.ArticlesData> _mAllNewsData = null;
  private Context _mContext = null;


  public VerticalViewPagerAdapter(FragmentManager fm, ArrayList<TopHeadLines.ArticlesData> _all_news_data, Context context) {
    super(fm);
    this._mAllNewsData = _all_news_data;
    this._mContext = context;
  }

  @Override
  public int getCount() {
    if (!ObjectUtil.isNull(_mAllNewsData) && _mAllNewsData.size() > 0)
      return _mAllNewsData.size();
    return 0;
  }

  @Override
  public Fragment getItem(int position) {
    if (position == 4) {
      ProfileFragment profileFragment = new ProfileFragment();
      Bundle bundle = new Bundle();
      bundle.putString("parent", String.valueOf(position));
      profileFragment.setArguments(bundle);
      return profileFragment;
    } else {
      TopHeadLines.ArticlesData single_news_data = _mAllNewsData.get(position);
      MainVerticalViewPagerFragment contentFragment = new MainVerticalViewPagerFragment();
      Bundle bundle = new Bundle();
      bundle.putParcelable("single_news_details", single_news_data);
      bundle.putString("parent", String.valueOf(position));
      contentFragment.setArguments(bundle);
      return contentFragment;
    }
  }
}
