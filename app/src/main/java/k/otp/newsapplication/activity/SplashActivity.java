package k.otp.newsapplication.activity;

import android.content.Intent;
import android.graphics.drawable.AnimationDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import butterknife.BindView;
import butterknife.ButterKnife;
import k.otp.newsapplication.MainActivity;
import k.otp.newsapplication.R;
import k.otp.newsapplication.utils.Constants;
import k.otp.newsapplication.utils.Helper;
import k.otp.newsapplication.utils.ObjectUtil;


public class SplashActivity extends AppCompatActivity {


  @BindView(R.id.background_rl)
  RelativeLayout background_rl;

  private AnimationDrawable animationDrawable;

  @Override
  protected void onCreate(@Nullable Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    requestWindowFeature(Window.FEATURE_NO_TITLE);
    getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN);
    setContentView(R.layout.splash_layout);
    ButterKnife.bind(this);

    animationDrawable = (AnimationDrawable) background_rl.getBackground();
    animationDrawable.setEnterFadeDuration(5000);
    animationDrawable.setExitFadeDuration(2000);
    new Handler().postDelayed(new Runnable() {
      @Override
      public void run() {


        if (Helper.loadArray(Constants.USER_CHOOSED_PREFRENCES_NAME, SplashActivity.this).size() > 0) {
          startActivity(new Intent(SplashActivity.this, PrefrencesSelection.class));
          finish();
        } else {
          startActivity(new Intent(SplashActivity.this, MainActivity.class));
          finish();
        }


      }
    }, 4000);

  }


  @Override
  protected void onResume() {
    super.onResume();
    if (!ObjectUtil.isNull(animationDrawable) && !animationDrawable.isRunning())
      animationDrawable.start();
  }

  @Override
  protected void onPause() {
    super.onPause();
    if (!ObjectUtil.isNull(animationDrawable) && animationDrawable.isRunning())
      animationDrawable.stop();
  }

}
