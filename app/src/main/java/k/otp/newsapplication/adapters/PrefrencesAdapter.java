package k.otp.newsapplication.adapters;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.v7.widget.AppCompatTextView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import k.otp.newsapplication.R;
import k.otp.newsapplication.utils.Constants;
import k.otp.newsapplication.utils.Helper;
import k.otp.newsapplication.utils.ObjectUtil;
import k.otp.newsapplication.utils.ResourcesUtil;
import timber.log.Timber;

/**
 * Created by sahitya on 7/10/18.
 */

public class PrefrencesAdapter extends RecyclerView.Adapter<PrefrencesAdapter.MyCustomViewHolder> {


  private ArrayList<String> _mAllPrefrences = null;
  private Context _mContext = null;
  private LayoutInflater _mLayoutInflater = null;


  public PrefrencesAdapter(Context context, ArrayList<String> allPrefrences) {
    this._mContext = context;
    this._mAllPrefrences = allPrefrences;
    this._mLayoutInflater = LayoutInflater.from(_mContext);
  }

  @NonNull
  @Override
  public MyCustomViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
    View itemView = _mLayoutInflater.inflate(R.layout.selection_prefrences_item_layout, parent, false);
    ButterKnife.bind(this, itemView);
    return new PrefrencesAdapter.MyCustomViewHolder(itemView);
  }

  @RequiresApi(api = Build.VERSION_CODES.JELLY_BEAN)
  @Override
  public void onBindViewHolder(@NonNull final MyCustomViewHolder holder, int position) {

    String single_selection = _mAllPrefrences.get(position);
    holder.pref_tv_itemlayout.setText(single_selection);

    if (single_selection.contentEquals(Constants.USER_PREF_ALL)) {
      Typeface font = Typeface.createFromAsset(_mContext.getAssets(), "fonts/all.otf");
      holder.pref_tv_itemlayout.setTypeface(font);
    } else if (single_selection.contentEquals(Constants.USER_PREF_SPORTS)) {
      Typeface font = Typeface.createFromAsset(_mContext.getAssets(), "fonts/sports.otf");
      holder.pref_tv_itemlayout.setTypeface(font);
    } else if (single_selection.contentEquals(Constants.USER_PREF_COMICS)) {
      Typeface font = Typeface.createFromAsset(_mContext.getAssets(), "fonts/comics.otf");
      holder.pref_tv_itemlayout.setTypeface(font);
    } else if (single_selection.contentEquals(Constants.USER_PREF_BUISNESS)) {
      Typeface font = Typeface.createFromAsset(_mContext.getAssets(), "fonts/buisness.otf");
      holder.pref_tv_itemlayout.setTypeface(font);
    } else if (single_selection.contentEquals(Constants.USER_PREF_ENTERTAINMENT)) {
      Typeface font = Typeface.createFromAsset(_mContext.getAssets(), "fonts/entertainment.otf");
      holder.pref_tv_itemlayout.setTypeface(font);
    } else {
      Typeface font = Typeface.createFromAsset(_mContext.getAssets(), "fonts/technology.otf");
      holder.pref_tv_itemlayout.setTypeface(font);
    }

    final ArrayList<String> user_pref = Helper.loadArray(Constants.USER_CHOOSED_PREFRENCES_NAME, _mContext);
    String selected_data = holder.pref_tv_itemlayout.getText().toString();

    /*
    * Changing the UI according to the pref. saved by the user*/
    if (user_pref.contains(selected_data)) {
      holder.pref_tv_itemlayout.setBackground(ResourcesUtil.getDrawableById(R.drawable.circular_selected_background));
      holder.pref_tv_itemlayout.setTextColor(ResourcesUtil.getColor(R.color.white));
    } else {
      holder.pref_tv_itemlayout.setBackground(ResourcesUtil.getDrawableById(R.drawable.circular_unselected_background));
      holder.pref_tv_itemlayout.setTextColor(ResourcesUtil.getColor(R.color.colorPrimaryDark));
    }

    holder.pref_tv_itemlayout.setOnClickListener(new View.OnClickListener() {
      @Override
      public void onClick(View v) {

        /*
        * Saving the pref. which is not in the db and if that is already in it than removing it from the pref. db
        *
        * Also refreshing the changes in ui with datasetchange
        * */


        String new_data = holder.pref_tv_itemlayout.getText().toString();
        if (!user_pref.contains(new_data)) {
          user_pref.add(user_pref.size(), new_data);
          Helper.saveArray(user_pref, Constants.USER_CHOOSED_PREFRENCES_NAME, _mContext);
          holder.pref_tv_itemlayout.setBackground(ResourcesUtil.getDrawableById(R.drawable.circular_selected_background));
          holder.pref_tv_itemlayout.setTextColor(ResourcesUtil.getColor(R.color.white));
          notifyDataSetChanged();
        } else {
          user_pref.remove(user_pref.indexOf(new_data));
          Helper.saveArray(user_pref, Constants.USER_CHOOSED_PREFRENCES_NAME, _mContext);
          holder.pref_tv_itemlayout.setBackground(ResourcesUtil.getDrawableById(R.drawable.circular_unselected_background));
          holder.pref_tv_itemlayout.setTextColor(ResourcesUtil.getColor(R.color.colorPrimaryDark));
          notifyDataSetChanged();
        }
      }
    });
  }

  @Override
  public int getItemCount() {
    if (!ObjectUtil.isNull(_mAllPrefrences) && _mAllPrefrences.size() > 0)
      return _mAllPrefrences.size();
    return 0;
  }

  public class MyCustomViewHolder extends RecyclerView.ViewHolder {
    @BindView(R.id.pref_tv_itemlayout)
    AppCompatTextView pref_tv_itemlayout;

    public MyCustomViewHolder(View itemView) {
      super(itemView);
      ButterKnife.bind(this, itemView);
    }
  }
}
